//module pattern
//budgetController = object
//IIFE - anonymous function:
//number and multiply function - private/safe/not accessible from outside(inside closure)
//multiplyTest - public
//Closure - publicTest has access to number and multply method even after returned
var budgetController = (function() {

                                        //private Expense contructor and prototype methods
   var Expense = function(id, description, amount)
   {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.percentage = -1; // assigned below
    };
    
    
     /* update percentage for each expense item in array
    Hypothetical scenario:
    expense = [a,b,c];
    item a - $10 -> 10/100 -> 10% of income
    item b - $20 -> 20/100 -> 20% of income
    item c - $40 -> 40/100 -> 40% of income        
    */
    //calculate percentage of an item for based on total income 
    Expense.prototype.calculatePercentage = function(totalIncome)
    {
        if(totalIncome > 0)
            this.percentage = Math.round( (this.amount / totalIncome) * 100);
        else this.percentage = -1;
    };
                                                    //getters
    Expense.prototype.getPercentage = function()
    {
       return this.percentage;  
    };

    //private Income constructor
    var Income = function(id, description, amount)
    {
        this.id = id;
        this.description = description;
        this.amount = amount;
    };
    
                                                    //private data structure
    var data =
    {
        allItems:
        {
            income: [],
            expense: []
        },
            
        totals:
        {
            income: 0,
            expense: 0
        },
        availableBalance: 0, //budget
        percentage: -1
    };
    
    //traverse income and expense array update respective totals
    var calculateAccountTotal = function(acccountType) // calculateTotal
    {
        var sum = 0;
        
        data.allItems[acccountType].forEach(function(currentElement){
            sum += currentElement.amount;
        });
        
        data.totals[acccountType] = sum;
    };
    
                                                        //PUBLIC METHODS
    return{
        //add new item to data structure and return it
        addNewItemToList: function(type, description, amount)
        {
            var ID, newItem;
            
            //assign id to new item
            if(data.allItems[type].length > 0) // array has items
                 ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            else // array empty
                ID = 0;
            
            //assign id the last length of array + 1
            //best case scenario (5 elements) = [0,1,2,3,4]; -> 5
            //real case scenario (5 elements) = [n,n,2,n,n,5,n,n,8,12,15] -> 5 XXXX
                        //array             last item             id    new id
            
            //create item - expense or income
            if(type === "expense")
                newItem = new Expense(ID, description, amount);
            else if(type === "income")
                newItem = new Income(ID, description, amount);
            
            //add item to data structure
            data.allItems[type].push(newItem);
            
            //return item
            return newItem;
        },
        /*
         Problem: we can't delete an item by its id in the array, b/c we can have an array of the following:
        [2,8,12] - so if we wanted to delete element 8 (id of 8) by using index 8 - it would be deleting the 8th element which doesn't exist
        we need to delete index 1 -> element 8
        
        Solution: Traverse the array of elements and return back map with element and its respective 
        
        */
        
        deleteItemFromList: function(itemType, itemId)
        {
            var indexOfItemToDelete;
            
            //traverse array and return a map - elements/their index in array
           var IDs = data.allItems[itemType].map(function(currentElement){
               return currentElement.id;
           });
            
            //assign the index of the element in map
            indexOfItemToDelete = IDs.indexOf(itemId);
            
            //delete item from array if exists
            if(indexOfItemToDelete !== -1)
                data.allItems[itemType].splice(indexOfItemToDelete, 1);
        },
        
        getAccountStatus: function()
        {
            return{
                totalIncome: data.totals.income,
                totalExpense: data.totals.expense,
                totalPercentageSpent: data.percentage,
                availableBalance: data.availableBalance
            };
        },
        getData: function()
        {
            return data;
        },
        
        //traverse expense array and store results in map<expense, expense percentage) and return it
        getExpensePercentages: function()
        {
           var expensePercentages = data.allItems.expense.map(function(currentElement){
               return currentElement.getPercentage();
            });
            return expensePercentages;
        },
        
        //update account status in data structures
        updateAccountStatus: function() //calculateBudget
        {
            //calculate total income and expenses
            calculateAccountTotal("expense");
            calculateAccountTotal("income");
            
            //calculate avaiable balance
            data.availableBalance = data.totals.income - data.totals.expense;
            
            //calculate percenage of income spent
            if(data.totals.income > 0)
                data.percentage = Math.round((data.totals.expense / data.totals.income) * 100);
            else 
                data.percentage = -1;
        },
        
        //traverse the expense array and updates percentage property for each element
        //percentage is originally set to -1
        updateExpensePercentages: function() // calculatePercentages
        {
            data.allItems.expense.forEach(function(currentElement){
                currentElement.calculatePercentage(data.totals.income);
            });
        }
    };
    
})();

// ui controller
var uiController = (function(){
    
    var domElements = 
        {
            //button
            addButton: '.add__btn',
            
            //inputs
            itemTypeInput: '.add__type', // income or expense
            itemDescriptionInput: '.add__description',
            itemAmountInput: '.add__value',
            
            //input css
            addTypeCss: '.add__type:focus',
            addTypeDescription: '.add__description:focus',
            addTypeValue: '.add__value:focus',
            
            //labels
            availableBalanceLabel: '.budget__value',
            itemPercentageLabel: '.item__percentage',
            totalIncomeLabel: '.budget__income--value',
            totalExpenseLabel: '.budget__expenses--value',
            percentageSpentLabel: '.budget__expenses--percentage',
            monthLabel: '.budget__title--month',
            
            //lists
            incomeList: '.income__list',
            expenseList: '.expenses__list',
            
            //item container
            itemContainer: '.container'
        };
    
                                                                    //PRIVATE FUNCTIONS
    
      var nodeListForEach = function(originalNodes, setPercentageLabels)
            {
                //loop through list of nodes
                for(var currentNode=0; currentNode < originalNodes.length; currentNode++)
                {
                    //                    percentage label     percentage calculated
                    setPercentageLabels(originalNodes[currentNode], currentNode);
                }
            };
    
    
     var formatCurrency = function(amount, type)
     {
        var amountSplit, integer, decimal, sign, newFormat;
        // + or - before number
        //exactly 2 decimal points
        //, for thousands

        amount = Math.abs(amount); // remove sign of number
        amount = amount.toFixed(2); // append .00

        amountSplit = amount.split('.'); // split amount and decimal into array


        //store integer and decimal
        integer = amountSplit[0];
        decimal = amountSplit[1];
         
        /*
        Add a coma after the first digit
        Subseqently append the last 2 digits
        When we evoke the first substring method the newly integer is not fully be assigned yet
        Similarly, when we append the ',' - newly integer has still not fully be assigned yet
        Finally, when we append the last 2 digits to the coma, then the integer has fully be assigned
        1234.5678
        12345.3789
        123456.7899
        */
        if(integer.length > 3) // 1,000 - 100,000
                                    //digits up to coma                 digits after comma till the end 
            integer = integer.substring(0,integer.length - 3) + ',' + integer.substr(integer.length - 3,3); // works 

        //check if income or expense and add sign

                                //sign                     number       decimals
        return (type === "income" ? "+" : "-") + ' $' + integer + '.' + decimal;            
    };
                                                        //PUBLIC METHODS
    return{
        
        addItemToList: function(newItem, type)
        {
            var originalItemString, newItemString, listType;
            
            //construct generic UI depending if type is "income" or "expense"
            
            if(type === "income")
                {
                    originalItemString = '<div class="item clearfix" id="income-%id%"> <div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%amount%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
                    
                    listType = domElements.incomeList;
                
                }
            else if(type === "expense")
                {
                    originalItemString = '<div class="item clearfix" id="expense-%id%"> <div class="item__description">%description%</div> <div class="right clearfix"><div class="item__value">%amount%</div> <div class="item__percentage">21%</div> <div class="item__delete">   <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div> </div> </div>';
                    
                    listType = domElements.expenseList;
                }
            
                    //set UI data for new item
                    /*
                    newItemString = original string but just replace the IDs
                    start with the string that just has the correct IDs, now just replace the description
                    start with the string that has the correct IDs AND description, now finally replace the amount
                    */
                    newItemString = originalItemString.replace("%id%", newItem.id);
                    newItemString = newItemString.replace("%description%", newItem.description);
                    newItemString = newItemString.replace("%amount%", formatCurrency(newItem.amount, type));
            
                    //insert new item into UI list - income or expense
                    document.querySelector(listType).insertAdjacentHTML('beforeend', newItemString);
        },
        
            /*
    This changes the input field and button borders from turquiose to red
    Each time the item type <+-> is changed this gets called. So at first the income is selected
    be default -> tourquise. Then the expense is selected -> red.
    */
        setItemTypeColor: function() // changedType
        {
            //get input fields
            var inputFields = document.querySelectorAll(
                domElements.itemTypeInput + ',' +
                domElements.itemDescriptionInput + ',' +
                domElements.itemAmountInput
            );
            
            
            
            //  apply/remove red focus css attribute to input fields
            nodeListForEach(inputFields, function(currentElement){
               currentElement.classList.toggle('red-focus'); 
            });
            
            // apply/remove red focus css attribute to button
            document.querySelector(domElements.addButton).classList.toggle('red');
        },
        
        clearInputdomPercentageLabels: function()
        {
            document.querySelector(domElements.itemDescriptionInput).value = '';
            document.querySelector(domElements.itemAmountInput).value = '';
            document.querySelector(domElements.itemDescriptionInput).focus();
        },
        
        displayMonth: function()
        {
            
            var months = ['January', 'Februrary', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                          'October', 'November', 'December'];
            
            
            var utcDate = new Date(); // date in UTC
            var monthInt = utcDate.getMonth();
            var monthString = months[monthInt];
            var year = utcDate.getFullYear();
            
            document.querySelector(domElements.monthLabel).textContent = monthString + " " + year;

        },
        
        getDomElements: function()
        {
            return domElements;
        },
        
        getUserInput: function(){
            return{
                itemType: document.querySelector(domElements.itemTypeInput).value, // income or expense
                itemDescription: document.querySelector(domElements.itemDescriptionInput).value,
                itemAmount: parseFloat(document.querySelector(domElements.itemAmountInput).value)
            };
        },
        
        removeElementFromDom: function(selectorId)
        {
            var elementToDelete = document.getElementById(selectorId);
            
            elementToDelete.parentElement.removeChild(elementToDelete);
        },
        
        //set the available balance, total income and expense, and percentage top area
        updateAccountCurrencyStatus: function(accountStatus)
        {
            //check if there is a positive or negative balance
            var type = accountStatus.availableBalance > 0 ? type = "income" : type = "expense";
            document.querySelector(domElements.availableBalanceLabel).textContent = 
                formatCurrency(accountStatus.availableBalance, type);
            
            document.querySelector(domElements.totalIncomeLabel).textContent = formatCurrency(accountStatus.totalIncome, "income");
            document.querySelector(domElements.totalExpenseLabel).textContent = formatCurrency(accountStatus.totalExpense, "expense");
            
            if(accountStatus.totalPercentageSpent > 0)
            document.querySelector(domElements.percentageSpentLabel).textContent = accountStatus.totalPercentageSpent + "%";
            else document.querySelector(domElements.percentageSpentLabel).textContent = "--";
        },
        
        
        /*
        Get a list of all the expense/percentages labels
        Traverse list and set their percentages        
        */
        updateAccountPercentageStatus: function(percentages) //displayPercentages
        {
            var domPercentageLabels = document.querySelectorAll(domElements.itemPercentageLabel); 
            console.log("percentages: " + domPercentageLabels);
            
            nodeListForEach(domPercentageLabels, function(currentPercentageLabel, index)
            {
                if(percentages[index] > 0)
                    currentPercentageLabel.textContent = percentages[index] + "%"; //assign percentage to percentage label
                else 
                    currentPercentageLabel.textContent = "n/a";
            });
        }
    }; //end of public functions
   
    
})();

//give different name for controllers in case we change the name of the module in the future
var appController = (function(budgetCtrl, UI_Controller){
    
                                                        // EVENT HANDLERS
    
   // (add item) event handler
    var addNewItemToList = function()
    {
        
        //get input
        var newItemDetails = UI_Controller.getUserInput();
        
        if( !isNaN(newItemDetails.itemAmount) && newItemDetails.itemAmount > 0 &&
            newItemDetails.itemDescription !== "")
            {
                //add item to data structure
                var newItem = budgetCtrl.addNewItemToList(newItemDetails.itemType, newItemDetails.itemDescription, newItemDetails.itemAmount);
                
                //add item to UI
                UI_Controller.addItemToList(newItem, newItemDetails.itemType);

                //clear input domPercentageLabels
                UI_Controller.clearInputdomPercentageLabels();

                //update budget in data and UI
                updateBudget();
                
                //update expense percentages in data and UI
                updateExpensePercentages();
            }  
    };
    
    /*
    (delete item) event handler
    The event is triggered on the container b/c both the income and expense list are wrapped in the container
    When we click the button - the event target is the button, but we don't want to delete
    the button element, rather we want to delete it's whole container that it is wrapped in. So we need to 
    traverse upwards (dom traversing) to reach its parent container. Again we specify the container to do the 
    event listening b/c both income and expense are inside the container. It is common to both.
    So when we click the button - it takes us to the button element which is:
    
    <button class="item__delete--btn">
          <i class="ion-ios-close-outline"></i>
     </button>
     
     But we need to traverse up the dom to get to:
     
     id="income-n" or id="expense-n" - respectively     
    */
    var deleteItemEventHandler = function(event)
    {
        var domSelectorWithId, domSelectorWithIdSplit, itemId, itemType;
        
        domSelectorWithId = event.target.parentNode.parentNode.parentNode.parentNode.id;
        
        // check if item exists
        if(domSelectorWithId) 
        {
            // if so split the item type with id
            domSelectorWithIdSplit = domSelectorWithId.split('-');
            itemType = domSelectorWithIdSplit[0]; // income/expense
            itemId = parseInt(domSelectorWithIdSplit[1]); // element ID
            
            // remove item from data structure
            budgetCtrl.deleteItemFromList(itemType, itemId);
            
            // remove item from UI
            uiController.removeElementFromDom(domSelectorWithId);
            
            // update account currency status (data and UI)
            updateBudget();
            
            //update account percentages status (data and UI)
            updateExpensePercentages();
            
        }
    };
                                                        //EVENT LISTENERS
    
    var setupEventListeners = function()
    {
        var domElements = UI_Controller.getDomElements();
        
        
                            //ADDING ITEM TO LIST
         //enter key 
        document.addEventListener('keypress', function(event){
        
        
        if(event.keyCode === 13 || event.which === 13)//added which for older browsers
            addNewItemToList();
        });
    
        //button
        document.querySelector(domElements.addButton).addEventListener('click', addNewItemToList);
        
        
                            //DELETING ITEM FROM LIST
        
        document.querySelector(domElements.itemContainer).addEventListener('click', deleteItemEventHandler);
        
        
                                //CHANGE INPUT FIELD COLORS
        document.querySelector(domElements.itemTypeInput).addEventListener('change', uiController.setItemTypeColor);
        
        document.querySelector(domElements.addButton).addEventListener('change', uiController.setItemTypeColor);
        
    };
                                                    // PRIVATE FUNCTIONS
    //update budget in data and UI
    var updateBudget = function()
    {
        //recalculate account status
        budgetCtrl.updateAccountStatus();
        
        //get status
        var accountStatus = budgetCtrl.getAccountStatus();

        //display new account status
        UI_Controller.updateAccountCurrencyStatus(accountStatus);
    };
    
    var updateExpensePercentages = function() // updatePercentages()
    {
        // calculate/update percentages
        // traverse expense array and set item's percentage property
        budgetCtrl.updateExpensePercentages();
        
        // retrieve percentages from the budget controller
        var percentages = budgetCtrl.getExpensePercentages();
        console.log("percentage amounts: " + percentages);
        
        // update percentages in UI then log to console
        uiController.updateAccountPercentageStatus(percentages);
        
    };
                                                            // PUBLIC METHODS
    return{
        init: function()
        {
            console.log('app has started');
            setupEventListeners();
            UI_Controller.updateAccountCurrencyStatus({
                totalIncome: 0,
                totalExpense: 0,
                totalPercentageSpent: -1,
                availableBalance: 0
            }); //clear all account status
            uiController.displayMonth();
        }
    };
    

    
    
})(budgetController, uiController);

appController.init();
